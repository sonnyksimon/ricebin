Alright, so we have

### [classical mechanics](https://en.wikipedia.org/wiki/Classical_mechanics)

### [electromagnetism](https://en.wikipedia.org/wiki/Electromagnetism)

### [relativity](https://en.wikipedia.org/wiki/Theory_of_relativity)

### [thermodynamics](https://en.wikipedia.org/wiki/Thermodynamics) and [statistical mechanics](https://en.wikipedia.org/wiki/Statistical_mechanics)

### [quantum mechanics](https://en.wikipedia.org/wiki/Quantum_mechanics)
