# The development of computers
  * ## outline the history of computers
    
    Raul Rojas, Ulf Hashagen (eds), The First Computers, MIT Press, Cambridge, 2000.
    Raul Rojas (ed.), Encyclopedia of Computers and Computer History, Fitzroy Deaborn, NY, 2001.
    
    http://www.inf.fu-berlin.de/lehre/SS01/hc/
    
    > 1. abacus
    > 2. pascals rotary machine (addition and subtraction)
    > 3. leibniz gear machine (multiplication and division)
    > 4. charles babbage and the difference engine
    > 5. charles babbage and the analytical engine
    > 6. harvard mark i (ibm)
    > 7. eniac
    > 8. konrad zuse's computers (germany)
    > 9. abc computer
    > 10. colossus and code breaking (uk)
    > 11. von neumann architecture (isa computer)
    > 12. first programming languages (plankalkul and FORTRAN)
    > 13. mainframes
    > 14. minicomputer revolution (1970s)
    > 15. personal computer (apple,ibm)
    > 16. arpanet and the internet
    > 17. www
    > 18. future of computing
    
  * ## explain the Von Neumann architecture
  
    https://www.computerscience.gcse.guru/theory/von-neumann-architecture
  
    > - control unit
    > - alu
    > - memory
    > - registers
    >   - memory address register
    >   - memory data register
    >   - accumulator
    >   - program counter
    >   - current instruction register
    > - i/o
    > - bus
    >   - address bus
    >   - data bus
    >   - control bus
    > - stored program concept
    >   - machine stores and executes instructions
    >   - instructions stored are built from predefined set of primitives
    >     - arithmetic and logic
    >     - simple tests/conditions
    >     - moving data around
    >   - special program (interpreter) executes instructions in order
    >     - tests/conditions change flow of executions/control through sequence
    >     - stops when done
    
  * ## explain the advantages of using the binary system in computing
  
    http://web.mit.edu/16.070/www/lecture/lecture_2_3.pdf
  
    > - transistors operate in two states // BellLabs
    > - signals are easily distinguishable
    > - less noise

# Data storage
  * ## outline the difference between electronic memory, magnetic memory and optical memory
    
    > all computation in memory is done in signals 1 and 0
    > - electronic memory
    >   - high voltage or electric current = 1
    >   - low voltage or no electric current = 0
    > - magnetic memory
    >   - magnetised spot = 1
    >   - unmagnetised spot = 0
    > - optical memory
    >   - current in photo-sensor = 1
    >     - lands - flat areas that directly reflect light into the photo-sensor
    >   - no current in photo-sensor = 0
    >     - pits - depressed areas that do not trigger photo-sensor

  * ## explain how data are stored in these three types of memories
  
    - all data is eventually converted to electric signal for computer to process
    - electronic memory
      - transistor composed of emitter, base and collector
      - correct voltage to base triggers current from collector to emitter
      - think of having current as 1, and 0 otherwise
      - logic gates built from transistors can represent complex conditions
      - flip flops built from logic gates store and remember data
    - magnetic memory
      - read/write head composed of coil of wire wound to iron former
      - magnetised spot induces current in coil (reading data)
      - big current in coil magnetises spot (storing data)
    - optical memory
      - disk composed of resin coated with reflective surface
      - photo-sensor reads data when light is reflected
      - low powered laser writes data by etching pits or 0s
  
  * ## explain how main memory is constructed using logic gates and how it is organised in terms of cells and addresses
  
  * ## explain the main terms concerning memory, e.g. memory capacity, access time, transfer rate
  
  * ## explain how the address decoder works

# Central processing unit
  * ## explain the components of the CPU and their functions
  
  * ## explain the instruction format and the instruction cycle (especially the fetch-execute cycle)
  
  * ## illustrate how the CPU executes instructions using a concrete example
  
  * ## explain the details of the execution of instructions, e.g. READ and WRITE, in terms of CPU registers and the system bus
  
  * ## explain the following ways of improving computer performance: clock frequency, cache memory, pipelining, CISC and RISC

# Data representation
  * ## explain how integers are represented in computers, e.g. using unsigned notation, signed magnitude notation, excess notation, and two's complement notation
  
  * ## explain how fractional numbers are represented in computers, e.g. using fixed-point notation and floating-point notation (especially the IEEE 754 single precision format)
  
  * ## calculate the decimal value represented by a binary sequence in unsigned notation, signed magnitude notation, excess notation, two's complement notation, and the IEEE 754 single precision floating-point format
  
  * ## when given a decimal number, determine the binary sequence that represents the number in unsigned notation, signed magnitude notation, excess notation, two's complement notation, and the IEEE 754 single precision floating-point format
  
  * ## explain how characters are represented in computers, e.g. using ASCII and Unicode
  
  * ## explain how colours, images, sound and movies are represented in computers

# Operating systems
  * ## describe the functions of the operating system and its major components
  
  * ## explain where the operating system is usually stored, and how it gets started
  
  * ## distinguish between multiprogramming and time-sharing operating systems
  
  * ## explain the concept of process, and describe the process control block associated with each process
  
  * ## explain the four different process queues: job queue, ready queue, intermediate queue, and I/O queues
  
  * ## explain how the long-term, short-term and medium-term schedulers operate on processes and the process queues
  
  * ## explain how the operating system manages memory by using techniques such as swapping, partitioning, paging, demand paging and virtual memory
  
  * ## explain how the operating system manages I/O devices using techniques such as programmed I/O, interrupt-driven I/O and direct memory access
  
  * ## explain the concept of interrupt, and describe how interrupts are caused received and handled

# Networking protocols and applications
  * ## explain the distinction between 'networks' and 'internets', and between 'logical' and 'physical' addresses of network components
  
  * ## explain what is meant by 'protocols' in the context of computer networking, and how protocols make it possible for applications on remote computers to communicate easily
  
  * ## explain the concept of 'layering' and describe in general terms the differences between the best-known layered models: OSI, TCP/IP and hybrid models

# TCP/IP internals
  * ## explain in general terms how information in TCP and IP headers is used in opening and closing connections
  
  * ## explain in general terms how information in TCP and IP headers is used in routing
  
  * ## explain in general terms how information in TCP and IP headers is used in flow control
  
  * ## explain in general terms how information in TCP and IP headers is used in reliable data transmission
  
  * ## identify different classes of IP addresses
  
  * ## break down an IP address into network, subnet and host IDs using a subnet mask
  
  * ## list the factors contributing to a shortage of IP addresses and some techniques which have been to alleviate it

# The world wide web and email
  * ## explain in general terms how web documents are transferred across the Internet and what processes are triggered when you click on a hyperlink
  
  * ## code web pages in XHTML using style sheets
  
  * ## explain why it is advisable to get into the habit of using XHTML rather than HTML
  
  * ## describe some of the technologies for dynamic web pages, and be able to select the appropriate type of technology for your requirements
  
  * ## describe the most widely used email protocols and summarise the important differences between them

# Legal framework
  * ## explain the difference between 'patent' and 'copyright', describe some ways in which internet technology has made it more difficult to enforce copyright or monitor copyright infringements, and discuss arguments for and against extending patent protection to computer software
  
  * ## list the 8 principles of data protection
  
  * ## explain what rights you have as a data subject in relation to persons or organisations holding your details on file
  
  * ## explain what companies must do to keep within the law if they keep records of individuals on manual or electronic files (assuming UK jurisdiction)
  
  * ## explain the legal implications of computer hacking, virus writing and unauthorised access to computer systems

# Hackers, viruses and security
  * ## explain the differences between viruses, worms and Trojan horses
  
  * ## describe some of the software vulnerabilities they exploit and the malicious techniques they use
  
  * ## advise on measures to avoid exposure to malicious code
  
  * ## explain in general terms how anti-virus software functions
