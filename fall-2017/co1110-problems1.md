# The development of computers
  * ## Is it possible to represent, store and process characters, colours, sound, movies and the like in a computer based on the decimal system, such as the ENIAC? If so, how would that work?
  
    https://electronics.howstuffworks.com/analog-digital.htm
    http://www.cs.scranton.edu/~cil102/data_sounds.html
    http://www.rit-mcsl.org/fairchild/WhyIsColor/Questions/4-5.html
    https://en.wikipedia.org/wiki/Decimal_computer
    https://en.wikipedia.org/wiki/ENIAC

    > For a decimal system such as the ENIAC:
    >  - alphanumeric characters could be represented by two decimal digits instead of one.
    >  - colors can be represented using the RGB scheme (3 decimal digits x3 = 9 decimal digits)
    >  - also streams of colors for each pixel can then be used to represent an image
    >  - the two factors to consider for digital sound representation are the sampling rate and precision. at sampling rates of 44100 samples per second and precision of 65536 gradations (quantization levels), the sound sounds perfect for most human ears. in a decimal system this would take 5 digits to represent 65536 gradations in the scope of 99999 gradations available
    >  - video can be thought of as frames of images that run at a certain rate. the frames are given a decimal number to store their sequence information. the rate, resolution, etc are stored separate from the actual video data in a metadata file. the data file then outputs each image onto the screen (by some ill-defined function) at the rate in the metadata, and increments the current sequence
    >  - movies are just video and audio which are synced up by an external file, or program (such as a codec) and then output at the same time to the user
